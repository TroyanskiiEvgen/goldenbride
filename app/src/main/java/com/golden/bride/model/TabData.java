package com.golden.bride.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.golden.bride.R;

/**
 * Created by Relax on 06.06.2017.
 */

public class TabData implements Parcelable{

    private int tabIconId;
    private int title;
    private int defaultColor;
    private int selectedColor;

    // TODO: 15.06.2017 remove this
    public TabData(int title) {
        defaultColor = R.color.gray;
        selectedColor = R.color.colorAccent;
        this.title = title;
    }


    public TabData(int iconId, int title, int defaultColor, int selectedColor) {
        this.tabIconId = iconId;
        this.title = title;
        this.defaultColor = defaultColor;
        this.selectedColor = selectedColor;
    }

    public int getTabIconId() {
        return tabIconId;
    }

    public int getTitle() {
        return title;
    }

    public int getDefaultColor() {
        return defaultColor;
    }

    public int getSelectedColor() {
        return selectedColor;
    }

    protected TabData(Parcel in) {
        tabIconId = in.readInt();
        title = in.readInt();
        defaultColor = in.readInt();
        selectedColor = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(tabIconId);
        dest.writeInt(title);
        dest.writeInt(defaultColor);
        dest.writeInt(selectedColor);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TabData> CREATOR = new Creator<TabData>() {
        @Override
        public TabData createFromParcel(Parcel in) {
            return new TabData(in);
        }

        @Override
        public TabData[] newArray(int size) {
            return new TabData[size];
        }
    };
}
