package com.golden.bride.model;

/**
 * Created by Relax on 17.06.2017.
 */

public class User {

    private long id;
    private String firstName;
    private String lastName;
    private String email;
    private String birthDay;
    private String country;
    private String profession;
    private String aboutMe;
}
