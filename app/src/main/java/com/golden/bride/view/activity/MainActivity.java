package com.golden.bride.view.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;

import com.golden.bride.R;
import com.golden.bride.view.fragment.FavoritesFragment;
import com.golden.bride.view.fragment.InboxFragment;
import com.golden.bride.view.fragment.LadiesFragment;
import com.golden.bride.view.fragment.ProfileFragment;
import com.golden.bride.view.fragment.LiveChatFragment;
import com.golden.bride.view.view.BottomNavigationBar;
import com.jetradar.multibackstack.BackStackActivity;
import com.golden.bride.model.TabData;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends BackStackActivity implements BottomNavigationBar.TabSelectListener {

    private static final String STATE_CURRENT_TAB_ID = "current_tab_id";
    private static final int MAIN_TAB_ID = 0;

    @BindView(R.id.bottom_nav_bar)
    BottomNavigationBar bottomNavBar;

    private Fragment curFragment;
    private int curTabId;

    @Override
    protected void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setUpBottomNavBar();

        if (state == null) {
            bottomNavBar.selectTab(MAIN_TAB_ID);
            showFragment(rootTabFragment(MAIN_TAB_ID));
        }
    }

    private void setUpBottomNavBar() {
        bottomNavBar
                .addItem(new TabData(R.drawable.ic_profile, R.string.profile, R.color.gray, R.color.colorAccent))
                .addItem(new TabData(R.drawable.ic_live_chat, R.string.live_chat, R.color.gray, R.color.colorAccent))
                .addItem(new TabData(R.color.light_gray, R.string.ladies, R.color.gray, R.color.colorAccent))
                .addItem(new TabData(R.drawable.ic_inbox, R.string.inbox, R.color.gray, R.color.colorAccent))
                .addItem(new TabData(R.drawable.ic_star, R.string.favorites, R.color.gray, R.color.colorAccent))
                .initialise();
        bottomNavBar.setTabSelectedListener(this);
    }

    @NonNull
    private Fragment rootTabFragment(int tabId) {
        switch (tabId) {
            case 0:
                return ProfileFragment.newInstance(1);
            case 1:
                return LiveChatFragment.newInstance(1);
            case 2:
                return LadiesFragment.newInstance();
            case 3:
                return InboxFragment.newInstance();
            case 4:
                return FavoritesFragment.newInstance(1);
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        curFragment = getSupportFragmentManager().findFragmentById(R.id.content);
        curTabId = savedInstanceState.getInt(STATE_CURRENT_TAB_ID);
        bottomNavBar.selectTab(curTabId);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_CURRENT_TAB_ID, curTabId);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        Pair<Integer, Fragment> pair = popFragmentFromBackStack();
        if (pair != null) {
            backTo(pair.first, pair.second);
        } else {
            super.onBackPressed();
        }
    }


    public void showFragment(@NonNull Fragment fragment) {
        showFragment(fragment, true);
    }

    public void showFragment(@NonNull Fragment fragment, boolean addToBackStack) {
        if (curFragment != null && addToBackStack) {
            pushFragmentToBackStack(curTabId, curFragment);
        }
        replaceFragment(fragment);
    }

    private void backTo(int tabId, @NonNull Fragment fragment) {
        if (tabId != curTabId) {
            curTabId = tabId;
            bottomNavBar.selectTab(curTabId);
        }
        replaceFragment(fragment);
        getSupportFragmentManager().executePendingTransactions();
    }

    private void backToRoot() {
        if (isRootTabFragment(curFragment, curTabId)) {
            return;
        }
        resetBackStackToRoot(curTabId);
        Fragment rootFragment = popFragmentFromBackStack(curTabId);
        assert rootFragment != null;
        backTo(curTabId, rootFragment);
    }

    private boolean isRootTabFragment(@NonNull Fragment fragment, int tabId) {
        return fragment.getClass() == rootTabFragment(tabId).getClass();
    }

    private void replaceFragment(@NonNull Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction tr = fm.beginTransaction();
        tr.replace(R.id.content, fragment);
        tr.commitAllowingStateLoss();
        curFragment = fragment;
    }


    @Override
    public void onTabSelected(int tabPosition) {
        if (curFragment != null) {
            pushFragmentToBackStack(curTabId, curFragment);
        }
        curTabId = tabPosition;
        Fragment fragment = popFragmentFromBackStack(curTabId);
        if (fragment == null) {
            fragment = rootTabFragment(curTabId);
        }
        replaceFragment(fragment);
    }
}