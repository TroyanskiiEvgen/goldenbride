package com.golden.bride.view.tamplate;

/**
 * Created by Relax on 17.06.2017.
 */

public interface ProfileViewInterface {

    void setData();

    void setFirstName();

    void setLastName();

    void setAboutMe();

    void setId();

    void setEmail();

    void setBirthDay();

    void setCountry();

    void setProfession();

    void setAvatar();;
}
