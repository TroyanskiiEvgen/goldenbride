package com.golden.bride.view.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.TextView;

import com.golden.bride.R;
import com.golden.bride.model.TabData;

import butterknife.BindView;

/**
 * Created by Relax on 06.06.2017.
 */

public class BottomNavigationTab extends BaseTab {

    @BindView(R.id.tab_icon) ImageView image;
    @BindView(R.id.tab_title) TextView title;

    public BottomNavigationTab(Context context, TabData tabData) {
        super(context, tabData);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.view_bottom_nav_tab;
    }


    @Override
    protected void setupViews(TabData tabData, boolean isSelected) {
        image.setImageResource(tabData.getTabIconId());
        image.setColorFilter(ContextCompat.getColor(getContext(),
                isSelected ? tabData.getSelectedColor() : tabData.getDefaultColor()));
        title.setText(tabData.getTitle());
        title.setTextColor(ContextCompat.getColor(getContext(),
                isSelected ? tabData.getSelectedColor() : tabData.getDefaultColor()));
    }
}
